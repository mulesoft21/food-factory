# FoodFactory Challenge

FoodFactory is a challenge excersice, event driven and threadsafe java application. For the solution desing, i used de next patterns:
- The pipeline pattern
- Task Parallelism pattern
- Producer - Consumer Pattern
- Template Method
- Composite
- Builder

In the docs folder you will find the diagrams that represent the desingn and modeling used for the solution:
- classdigram1.0.png
- event-driven-process.png
- food-factory-flow.png

### Stack

* Java 8
* Maven 3
* Junit 5

### Installation

FoodFactory requires [Java 8](https://www.oracle.com/ar/java/technologies/javase/javase-jdk8-downloads.html) and [Maven 3](https://maven.apache.org/download.cgi) to run

To run a POC of challenge, execute the next in terminal:

```shFoodFactory is a challenge exercise, event driven and thread safe java application. For the solution design, i used the next patterns:
$ cd food-factory
$ mvn clean dependency:copy-dependencies package -DskipTests
$ java -jar target/food-factory-1.0.0.jar
```

To run junit test, execute the next in terminal:

```shFoodFactory is a challenge exercise, event driven and thread safe java application. For the solution design, i used then next patterns:
$ cd food-factory
$ mvn test
```

### Todos

 - Write more Tests
 - Add multichannel Oven & Store
 

 
 &copy; Jonatan Aguilar - Java Software Developer
