package com.sms;

import com.sms.domain.FoodFactoryPipeline;

/**
 * A factory for creating Food objects.
 */
public class FoodFactory 
{
	
	/** The Constant M_OVENS. */
	private static final int M_OVENS = 3;
	
	/** The Constant N_STORES. */
	private static final int N_STORES = 3;
	
	/** The Constant OVEN_CAPACITY. */
	private static final double OVEN_CAPACITY = 100;
	
	/** The Constant STORE_CAPACITY. */
	private static final int STORE_CAPACITY = 20;
	
	/** The Constant CHANNEL_BUS_CAPACITY. */
	private static final int CHANNEL_BUS_CAPACITY = 10;

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main( String[] args )
    {
    	FoodFactoryPipeline p = new FoodFactoryPipeline.Builder()
    			.withChannelBusCapacity(CHANNEL_BUS_CAPACITY)
    			.withOvenCapacity(OVEN_CAPACITY)
    			.withStoreCapacity(STORE_CAPACITY)
    			.withStoresCount(N_STORES)
    			.withOvensCount(M_OVENS)
    			.build();
    	
    	p.start();
    }
}
