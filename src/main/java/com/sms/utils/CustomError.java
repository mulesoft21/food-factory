package com.sms.utils;

/**
 * The Enum CustomError.
 */
public enum CustomError {
	
	/** The stage error. */
	STAGE_ERROR(1, "Stage Error");
	
	/** The code. */
	private final int code;
	
	/** The description. */
	private final String description;

	/**
	 * Instantiates a new custom error.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private CustomError(int code, String description) {
	    this.code = code;
	    this.description = description;
	}

	/**
	 * Gets the description.
	 *
	 * @param varargs the varargs
	 * @return the description
	 */
	public String getDescription(Object...varargs) {
		return String.format(description, varargs);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return code + ": " + getDescription();
	}

}
