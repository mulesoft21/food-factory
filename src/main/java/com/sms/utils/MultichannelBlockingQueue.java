package com.sms.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * The Class MultichannelBlockingQueue.
 *
 * @param <E> the element type
 */
public class MultichannelBlockingQueue<E>{

	/** The channels. */
	private Map<String, BlockingQueue<E>> channels = new HashMap<>();
	
	/**
	 * Instantiates a new multichannel blocking queue.
	 *
	 * @param maxSize the max size
	 * @param channelsNames the channels names
	 */
	public MultichannelBlockingQueue(int maxSize, String[] channelsNames) {
		Arrays.stream(channelsNames).forEach((c) -> channels.put(c, new LinkedBlockingDeque<>(maxSize)));
	}
	
	/**
	 * Put.
	 *
	 * @param e the e
	 * @param channel the channel
	 * @throws InterruptedException the interrupted exception
	 */
	public void put(E e, String channel) throws InterruptedException {
		channels.get(channel).put(e);
	}

	/**
	 * Take.
	 *
	 * @param channel the channel
	 * @return the e
	 * @throws InterruptedException the interrupted exception
	 */
	public E take(String channel) throws InterruptedException {
		return channels.get(channel).take();
	}
	
	/**
	 * Checks if is empty.
	 *
	 * @param channel the channel
	 * @return true, if is empty
	 */
	public boolean isEmpty(String channel) {
		return channels.get(channel).isEmpty();
	}
	
	/**
	 * Gets the size.
	 *
	 * @param channel the channel
	 * @return the size
	 */
	public int getSize(String channel) {
		return channels.get(channel).size();
	}
	
	/**
	 * Gets the channel count.
	 *
	 * @return the channel count
	 */
	public int getChannelCount() {
		return channels.keySet().size();
	}

}
