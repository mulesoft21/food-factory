package com.sms.domain;

import com.sms.exceptions.AssemblyStageException;
import com.sms.utils.MultichannelBlockingQueue;

/**
 * The Interface PipelineStage.
 *
 * @param <I> the generic type
 * @param <O> the generic type
 */
public interface PipelineStage<I, O> extends Runnable{
	
	/**
	 * On start.
	 */
	void onStart();
	
	/**
	 * On running.
	 *
	 * @throws AssemblyStageException the assembly stage exception
	 */
	void onRunning() throws AssemblyStageException;
	
	/**
	 * On finish.
	 */
	void onFinish();
	
	/**
	 * Inits the.
	 *
	 * @param inputBuffer the input buffer
	 * @param outputBuffer the output buffer
	 * @param inputChannel the input channel
	 * @param ouputChannel the ouput channel
	 */
	void init(MultichannelBlockingQueue<TrackeableMessage<I>> inputBuffer, MultichannelBlockingQueue<TrackeableMessage<O>> outputBuffer, String inputChannel, String ouputChannel);
	
	/**
	 * Handle pipeline exception.
	 *
	 * @param e the e
	 */
	void handlePipelineException(Exception e);
	
	/**
	 * Gets the input channel.
	 *
	 * @return the input channel
	 */
	String getInputChannel();
	
	/**
	 * Gets the output channel.
	 *
	 * @return the output channel
	 */
	String getOutputChannel();
}
