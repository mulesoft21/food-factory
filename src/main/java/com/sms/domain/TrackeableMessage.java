package com.sms.domain;

/**
 * The Class TrackeableMessage.
 *
 * @param <T> the generic type
 */
public class TrackeableMessage<T> {

	/** The content. */
	T content;
	
	/** The channel. */
	String channel;

	/**
	 * Instantiates a new trackeable message.
	 *
	 * @param content the content
	 * @param channel the channel
	 */
	public TrackeableMessage(T content, String channel) {
		super();
		this.content = content;
		this.channel = channel;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public T getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(T content) {
		this.content = content;
	}

	/**
	 * Gets the channel.
	 *
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * Sets the channel.
	 *
	 * @param channel the new channel
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	
}
