package com.sms.domain;

import java.time.LocalTime;

/**
 * The Interface Cookeable.
 */
public interface Cookeable {
	
	/**
	 * Checks if is cooked.
	 *
	 * @return true, if is cooked
	 */
	boolean isCooked();
	
	/**
	 * Gets the cooking start time.
	 *
	 * @return the cooking start time
	 */
	LocalTime getCookingStartTime();

	/**
	 * Sets the cooking start time.
	 *
	 * @param cookingStartTime the new cooking start time
	 */
	void setCookingStartTime(LocalTime cookingStartTime);
}
