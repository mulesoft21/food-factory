package com.sms.domain;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.exceptions.AssemblyStageException;
import com.sms.utils.MultichannelBlockingQueue;

/**
 * The Class BaseAssemblyLineStage.
 *
 * @param <I> the generic type
 * @param <O> the generic type
 */
public abstract class BaseAssemblyLineStage<I, O> implements AssemblyLineStage<I, O> {

	/** The input buffer. */
	MultichannelBlockingQueue<TrackeableMessage<I>> inputBuffer;

	/** The output buffer. */
	MultichannelBlockingQueue<TrackeableMessage<O>> outputBuffer;

	/** The input channel. */
	String inputChannel;
	
	/** The output channel. */
	String outputChannel;

	/** The done. */
	boolean done;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BaseAssemblyLineStage.class);
	
	/**
	 * Put after.
	 *
	 * @param item the item
	 * @throws AssemblyStageException the assembly stage exception
	 */
	@Override
	public void putAfter(TrackeableMessage<O> item) throws AssemblyStageException {
		try {
			outputBuffer.put(item, Optional.ofNullable(getOutputChannel()).orElse(item.getChannel()));
		} catch (InterruptedException e) {
			throw new AssemblyStageException(e);
		}
	}

	/**
	 * Take.
	 *
	 * @return the trackeable message
	 * @throws AssemblyStageException the assembly stage exception
	 */
	@Override
	public TrackeableMessage<I> take() throws AssemblyStageException {
		try {
			LOGGER.info(Thread.currentThread().getName() + " TAKE()");
			return inputBuffer.take(getInputChannel());
		} catch (InterruptedException e) {
			throw new AssemblyStageException(e);
		}
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {
		try {
			onStart();
			while (!done) {
				onRunning();
			}
			onFinish();
		} catch (Exception e) {
			handlePipelineException(e);
		}
	}

	/**
	 * Inits the.
	 *
	 * @param inputBuffer the input buffer
	 * @param outputBuffer the output buffer
	 * @param inputChannel the input channel
	 * @param outputChannel the output channel
	 */
	@Override
	public void init(MultichannelBlockingQueue<TrackeableMessage<I>> inputBuffer, MultichannelBlockingQueue<TrackeableMessage<O>> outputBuffer, String inputChannel, String outputChannel) {
		this.inputBuffer = inputBuffer;
		this.outputBuffer = outputBuffer;
		this.inputChannel = inputChannel;
		this.outputChannel = outputChannel;
	}
	
	/**
	 * Gets the input channel.
	 *
	 * @return the input channel
	 */
	@Override
	public String getInputChannel() {
		return inputChannel;
	};
	
	/**
	 * Gets the output channel.
	 *
	 * @return the output channel
	 */
	@Override
	public String getOutputChannel() {
		return outputChannel;
	};

}
