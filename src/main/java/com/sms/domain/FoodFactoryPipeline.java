package com.sms.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import com.sms.domain.impl.CookingStageImpl;
import com.sms.domain.impl.MultiStoreImpl;
import com.sms.domain.impl.OvenImpl;
import com.sms.domain.impl.ProductGeneratorStageImpl;
import com.sms.domain.impl.ServingFoodStageImpl;
import com.sms.domain.impl.SimpleStoreImpl;
import com.sms.utils.MultichannelBlockingQueue;

/**
 * The Class FoodFactoryPipeline.
 */
public class FoodFactoryPipeline {
	
	/** The stages. */
	private List<PipelineStage<Product, Product>> stages;
	
	/** The ovens count. */
	private int ovensCount;
	
	/** The stores count. */
	private int storesCount;
	
	/** The oven capacity. */
	private double ovenCapacity;
	
	/** The store capacity. */
	private int storeCapacity;
	
	/** The channel bus capacity. */
	private int channelBusCapacity;
	
	/**
	 * The Class Builder.
	 */
	public static class Builder {

		/** The Constant PIPELINE_CHANNEL1. */
		private static final String PIPELINE_CHANNEL1 = "PIPELINE_CHANNEL1";
		
		/** The Constant PIPELINE_CHANNEL2. */
		private static final String PIPELINE_CHANNEL2 = "PIPELINE_CHANNEL2";
		
		/** The Constant PIPELINE_COMMON. */
		private static final String PIPELINE_COMMON = "PIPELINE_COMMON";
		
		/** The ovens count. */
		private int ovensCount;
		
		/** The stores count. */
		private int storesCount;
		
		/** The oven capacity. */
		private double ovenCapacity;
		
		/** The store capacity. */
		private int storeCapacity;
		
		/** The channel bus capacity. */
		private int channelBusCapacity;
		
		
		/**
		 * With ovens count.
		 *
		 * @param ovensCount the ovens count
		 * @return the builder
		 */
		public Builder withOvensCount(int ovensCount) {
			this.ovensCount = ovensCount;
			return this;
		}

		/**
		 * With stores count.
		 *
		 * @param storesCount the stores count
		 * @return the builder
		 */
		public Builder withStoresCount(int storesCount) {
			this.storesCount = storesCount;
			return this;
		}

		/**
		 * With oven capacity.
		 *
		 * @param ovenCapacity the oven capacity
		 * @return the builder
		 */
		public Builder withOvenCapacity(double ovenCapacity) {
			this.ovenCapacity = ovenCapacity;
			return this;
		}

		/**
		 * With store capacity.
		 *
		 * @param storeCapacity the store capacity
		 * @return the builder
		 */
		public Builder withStoreCapacity(int storeCapacity) {
			this.storeCapacity = storeCapacity;
			return this;
		}

		/**
		 * With channel bus capacity.
		 *
		 * @param channelBusCapacity the channel bus capacity
		 * @return the builder
		 */
		public Builder withChannelBusCapacity(int channelBusCapacity) {
			this.channelBusCapacity = channelBusCapacity;
			return this;
		}
		
		/**
		 * Builds the.
		 *
		 * @return the food factory pipeline
		 */
		public FoodFactoryPipeline build(){
			FoodFactoryPipeline instance = new FoodFactoryPipeline();

			PipelineStage<Product, Product> productGeneratorStageAL1 = new ProductGeneratorStageImpl();
			PipelineStage<Product, Product> productGeneratorStageAL2 = new ProductGeneratorStageImpl();
			PipelineStage<Product, Product> cookingStage = new CookingStageImpl(createOvens(), new MultiStoreImpl<TrackeableMessage<Product>>(createStores()));
			PipelineStage<Product, Product> servingFoodStageAL1 = new ServingFoodStageImpl();
			PipelineStage<Product, Product> servingFoodStageAL2 = new ServingFoodStageImpl();
			
			String[] channelsNamesBus1 = {PIPELINE_COMMON};
			String[] channelsNamesBus2 = {PIPELINE_CHANNEL1, PIPELINE_CHANNEL2};
			
			MultichannelBlockingQueue<TrackeableMessage<Product>> bus1 = new MultichannelBlockingQueue<>(channelBusCapacity, channelsNamesBus1);
			MultichannelBlockingQueue<TrackeableMessage<Product>> bus2 = new MultichannelBlockingQueue<>(channelBusCapacity, channelsNamesBus2);

			productGeneratorStageAL1.init(null, bus1, PIPELINE_CHANNEL1, PIPELINE_COMMON);
			productGeneratorStageAL2.init(null, bus1, PIPELINE_CHANNEL2, PIPELINE_COMMON);
			cookingStage.init(bus1, bus2, PIPELINE_COMMON, null);
			servingFoodStageAL1.init(bus2, null, PIPELINE_CHANNEL1, PIPELINE_CHANNEL1);
			servingFoodStageAL2.init(bus2, null, PIPELINE_CHANNEL2, PIPELINE_CHANNEL2);
			
			List<PipelineStage<Product, Product>> stages = new ArrayList<>();
			stages.add(productGeneratorStageAL1);
			stages.add(productGeneratorStageAL2);
			stages.add(cookingStage);
			stages.add(servingFoodStageAL1);
			stages.add(servingFoodStageAL2);
			
			instance.stages = stages;
			instance.channelBusCapacity = channelBusCapacity;
			instance.ovenCapacity = ovenCapacity;
			instance.ovensCount = ovensCount;
			instance.storeCapacity = storeCapacity;
			instance.storesCount = storesCount;
			
			return instance;
		}
		
		/**
		 * Creates the ovens.
		 *
		 * @return the oven[]
		 */
		@SuppressWarnings("unchecked")
		private  Oven<TrackeableMessage<Product>>[] createOvens() {
			Oven<TrackeableMessage<Product> >[] ovens = new Oven[ovensCount];
			IntStream.range(0, ovensCount).forEach((idx) -> ovens[idx] = new OvenImpl(ovenCapacity));
			return ovens;
		}
		
		/**
		 * Creates the stores.
		 *
		 * @return the store[]
		 */
		@SuppressWarnings("unchecked")
		private Store<TrackeableMessage<Product> >[] createStores() {
			Store<TrackeableMessage<Product>>[] stores = new Store[storesCount];
			IntStream.range(0, storesCount).forEach((idx) -> stores[idx] = new SimpleStoreImpl(storeCapacity));
			return stores;
		}
	}	
	
	/**
	 * Instantiates a new food factory pipeline.
	 */
	private FoodFactoryPipeline() {}
	
	/**
	 * Start.
	 */
	public void start() {
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		stages.forEach((stage)-> executorService.execute(stage));
	}

	/**
	 * Gets the ovens count.
	 *
	 * @return the ovens count
	 */
	public int getOvensCount() {
		return ovensCount;
	}

	/**
	 * Gets the stores count.
	 *
	 * @return the stores count
	 */
	public int getStoresCount() {
		return storesCount;
	}

	/**
	 * Gets the oven capacity.
	 *
	 * @return the oven capacity
	 */
	public double getOvenCapacity() {
		return ovenCapacity;
	}

	/**
	 * Gets the store capacity.
	 *
	 * @return the store capacity
	 */
	public int getStoreCapacity() {
		return storeCapacity;
	}

	/**
	 * Gets the channel bus capacity.
	 *
	 * @return the channel bus capacity
	 */
	public int getChannelBusCapacity() {
		return channelBusCapacity;
	}
}
