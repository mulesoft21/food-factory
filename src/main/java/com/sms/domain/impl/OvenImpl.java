package com.sms.domain.impl;

import java.time.Duration;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.domain.Oven;
import com.sms.domain.Product;
import com.sms.domain.TrackeableMessage;
import com.sms.exceptions.CapacityExceededException;

/**
 * The Class OvenImpl.
 */
public class OvenImpl implements Oven<TrackeableMessage<Product>> {

	/** The size. */
	private double size;
	
	/** The used size. */
	private double usedSize;
	
	/** The slots. */
	private List<TrackeableMessage<Product>> slots;
		
	/** The on. */
	private boolean on;
	
	/** The duration. */
	private Duration duration;
	
	/** The start time. */
	private LocalTime startTime;
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(OvenImpl.class);
	
	/**
	 * Instantiates a new oven impl.
	 *
	 * @param size the size
	 */
	public OvenImpl(double size) {
		super();
		this.size = size;
		this.slots = new LinkedList<>();
		this.usedSize = 0.0;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@Override
	public double getSize() {
		return size;
	}

	/**
	 * Put.
	 *
	 * @param item the item
	 * @throws CapacityExceededException the capacity exceeded exception
	 */
	@Override
	synchronized public void put(TrackeableMessage<Product> item) throws CapacityExceededException {
		double newSize = usedSize + item.getContent().getSize();
		if (newSize <= size) {
			item.getContent().setCookingStartTime(LocalTime.now());
			slots.add(item);
			usedSize += item.getContent().getSize();
		} else {
			throw new CapacityExceededException();
		}
	}

	/**
	 * Take.
	 *
	 * @param item the item
	 * @return true, if successful
	 */
	@Override
	synchronized public boolean take(TrackeableMessage<Product> item) {
		boolean success = slots.removeIf((i) -> item.equals(i));
		if(success && slots.isEmpty()) {
			usedSize -= item.getContent().getSize();
			turnOff();
		}
		
		return success;
	}

	/**
	 * Turn on.
	 */
	@Override
	synchronized public void turnOn() {
		turnOn(null);
	}
	
	/**
	 * Turn on.
	 *
	 * @param duration the duration
	 */
	@Override
	synchronized public void turnOn(Duration duration) {
		this.duration = this.duration != null ? this.duration.plus(duration) : duration;
		
		if(!on) {
			this.on = true;
			this.startTime = LocalTime.now();
			
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			
			executorService.execute(new Thread(()->{
				while(on && !isExpired()) {
					LOGGER.info(Thread.currentThread().getName() + " OVEN BURNING");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						LOGGER.error(e);
					}
				}
				turnOff();
			}));
		}
	}

	/**
	 * Turn off.
	 */
	@Override
	synchronized public void turnOff() {
		on = false;
		usedSize = 0.0;
		LOGGER.info(Thread.currentThread().getName() + " OVEN TURN OFF");
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}
	
	/**
	 * Checks if is expired.
	 *
	 * @return true, if is expired
	 */
	private boolean isExpired() {
		return duration != null && duration.compareTo(Duration.between(startTime, LocalTime.now())) <= 0;
	}
	
	/**
	 * Checks if is on.
	 *
	 * @return true, if is on
	 */
	@Override
	public boolean isOn() {
		return on;
	}

}
