package com.sms.domain.impl;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.javafaker.Faker;
import com.sms.domain.BaseAssemblyLineStage;
import com.sms.domain.Product;
import com.sms.domain.TrackeableMessage;
import com.sms.exceptions.AssemblyStageException;
import com.sms.utils.CustomError;

/**
 * The Class ProductGeneratorStageImpl.
 */
public class ProductGeneratorStageImpl extends BaseAssemblyLineStage<Product, Product> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(ProductGeneratorStageImpl.class);

	/** The faker. */
	private Faker faker = new Faker(new Locale("en-US"));

	/**
	 * On start.
	 */
	@Override
	public void onStart() {
		LOGGER.info(Thread.currentThread().getName() + " STARTING STAGE WORKER");
	}

	/**
	 * On running.
	 */
	@Override
	public void onRunning() {
		LOGGER.info(Thread.currentThread().getName() + " RUNNING STAGE WORKER");

		LOGGER.info(Thread.currentThread().getName() + " GENERATING PRODUCT");

		Product currentProduct = new ProductImpl(Duration.of(faker.number().numberBetween(1, 3), ChronoUnit.SECONDS),
				faker.number().randomDouble(2, 10, 100), faker.food().spice());

		try {
			Thread.sleep(1000);
			putAfter(new TrackeableMessage<Product>(currentProduct, getInputChannel()));
		} 
		catch (InterruptedException e) {
			handlePipelineException(e);
		}
		catch (AssemblyStageException e) {
			handlePipelineException(e);
		}
	}

	/**
	 * On finish.
	 */
	@Override
	public void onFinish() {
		LOGGER.info(Thread.currentThread().getName() + " FINISHING STAGE WORKER");
	}

	/**
	 * Handle pipeline exception.
	 *
	 * @param e the e
	 */
	@Override
	public void handlePipelineException(Exception e) {
		LOGGER.error(CustomError.STAGE_ERROR, e);
	}
}
