package com.sms.domain.impl;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.domain.BaseAssemblyLineStage;
import com.sms.domain.Oven;
import com.sms.domain.Product;
import com.sms.domain.Store;
import com.sms.domain.TrackeableMessage;
import com.sms.exceptions.AssemblyStageException;
import com.sms.exceptions.CapacityExceededException;
import com.sms.utils.CustomError;

/**
 * The Class CookingStageImpl.
 */
public class CookingStageImpl extends BaseAssemblyLineStage<Product, Product> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(CookingStageImpl.class);

	/** The ovens. */
	private Oven<TrackeableMessage<Product>>[] ovens;

	/** The store. */
	private Store<TrackeableMessage<Product>> store;

	/** The stage cooking items. */
	private Queue<TrackeableMessage<Product>> stageCookingItems;

	/** The stage store items. */
	private Queue<TrackeableMessage<Product>> stageStoreItems;

	/**
	 * Instantiates a new cooking stage impl.
	 *
	 * @param ovens the ovens
	 * @param store the store
	 */
	public CookingStageImpl(Oven<TrackeableMessage<Product>>[] ovens, Store<TrackeableMessage<Product>> store) {
		super();
		this.ovens = ovens;
		this.store = store;
		stageCookingItems = new LinkedList<TrackeableMessage<Product>>();
		stageStoreItems = new LinkedList<TrackeableMessage<Product>>();
	}

	/**
	 * On start.
	 */
	@Override
	public void onStart() {
		LOGGER.info(Thread.currentThread().getName() + " STARTING STAGE WORKER");
	}

	/**
	 * On running.
	 *
	 * @throws AssemblyStageException the assembly stage exception
	 */
	@Override
	public void onRunning() throws AssemblyStageException {
		LOGGER.info(Thread.currentThread().getName() + " RUNNING STAGE WORKER");

		TrackeableMessage<Product> currentItem = Optional.ofNullable(takeFromStore()).orElse(take());
		
		if (!putInOven(currentItem)) {
			putInStores(currentItem);
		}

		TrackeableMessage<Product> cookedProduct = takeFromOvens();

		if (cookedProduct != null) {
			putAfter(currentItem);
		}
		
		putAfter(currentItem);
	}

	/**
	 * On finish.
	 */
	@Override
	public void onFinish() {
		LOGGER.info(Thread.currentThread().getName() + " FINISHING STAGE WORKER");
	}

	/**
	 * Handle pipeline exception.
	 *
	 * @param e the e
	 */
	@Override
	public void handlePipelineException(Exception e) {
		LOGGER.error(CustomError.STAGE_ERROR, e);
	}

	/**
	 * Take from ovens.
	 *
	 * @return the trackeable message
	 */
	synchronized private TrackeableMessage<Product> takeFromOvens() {
		final TrackeableMessage<Product> targetItem = stageCookingItems.peek();

		if (targetItem != null && targetItem.getContent().isCooked()) {
			boolean success = false;
			int idx = 0;
			while(idx < ovens.length && !success) {
				success = ovens[idx].take(targetItem);
				idx++;
			}
			
			if(success) {
				return stageCookingItems.remove();
			}
		}

		return null;
	}

	/**
	 * Take from store.
	 *
	 * @return the trackeable message
	 */
	synchronized private TrackeableMessage<Product> takeFromStore() {
		TrackeableMessage<Product> targetItem = stageStoreItems.peek();
		if (targetItem != null && store.take(targetItem)) {
			return stageStoreItems.remove();
		}
		return targetItem;
	}

	/**
	 * Put in stores.
	 *
	 * @param product the product
	 */
	synchronized private void putInStores(TrackeableMessage<Product> product) {
		store.put(product);
		stageStoreItems.add(product);
	}

	/**
	 * Put in oven.
	 *
	 * @param item the item
	 * @return true, if successful
	 */
	synchronized private boolean putInOven(TrackeableMessage<Product> item) {
		LOGGER.info(Thread.currentThread().getName() + " COOKING PRODUCT");

		int idx = 0;
		boolean loop = idx < ovens.length;
		boolean success = false;
		while (loop) {
			try {
				ovens[idx].put(item);
				ovens[idx].turnOn(item.getContent().getCookTime());
				stageCookingItems.add(item);
				loop = false;
				success = true;
			} catch (CapacityExceededException e) {
				LOGGER.info(e);
				idx++;
				loop = idx < ovens.length;
			}
		}

		return success;
	}
}
