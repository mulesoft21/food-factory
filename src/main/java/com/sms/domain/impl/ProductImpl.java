package com.sms.domain.impl;

import java.time.Duration;
import java.time.LocalTime;

import com.sms.domain.Product;

/**
 * The Class ProductImpl.
 */
public class ProductImpl implements Product{
	
	/** The duration. */
	private Duration duration;
	
	/** The size. */
	private double size;
	
	/** The name. */
	private String name;
	
	/** The cooking start time. */
	LocalTime cookingStartTime;
	
	/**
	 * Instantiates a new product impl.
	 *
	 * @param duration the duration
	 * @param size the size
	 * @param name the name
	 */
	public ProductImpl(Duration duration, double size, String name) {
		super();
		this.duration = duration;
		this.size = size;
		this.name = name;
	}

	/**
	 * Gets the cook time.
	 *
	 * @return the cook time
	 */
	@Override
	public Duration getCookTime() {
		return duration;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@Override
	public double getSize() {
		return size;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ProductImpl [duration=" + duration.getSeconds() + ", size=" + size + ", name=" + name + "]";
	}

	/**
	 * Checks if is cooked.
	 *
	 * @return true, if is cooked
	 */
	@Override
	public boolean isCooked() {
		return getCookTime().compareTo(Duration.between(getCookingStartTime(), LocalTime.now())) <= 0;
	}

	/**
	 * Gets the cooking start time.
	 *
	 * @return the cooking start time
	 */
	@Override
	public LocalTime getCookingStartTime() {
		return cookingStartTime;
	}

	/**
	 * Sets the cooking start time.
	 *
	 * @param cookingStartTime the new cooking start time
	 */
	@Override
	public void setCookingStartTime(LocalTime cookingStartTime) {
		this.cookingStartTime = cookingStartTime;
	}
}
