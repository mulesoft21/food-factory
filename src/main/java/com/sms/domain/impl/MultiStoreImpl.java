package com.sms.domain.impl;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.domain.Store;

/**
 * The Class MultiStoreImpl.
 *
 * @param <T> the generic type
 */
public class MultiStoreImpl<T> implements Store<T>{
	
	/** The stores. */
	private Store<T>[] stores;
	
	/** The lock. */
	private ReentrantLock lock = new ReentrantLock();
	
    /** The not full condition. */
    private Condition notFullCondition = lock.newCondition();
    
    /** The not empty condition. */
    private Condition notEmptyCondition = lock.newCondition();
    
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(MultiStoreImpl.class);
	
	/**
	 * Instantiates a new multi store impl.
	 *
	 * @param stores the stores
	 */
	public MultiStoreImpl(Store<T>[] stores) {
		super();
		this.stores = stores;
	}

	/**
	 * Put.
	 *
	 * @param item the item
	 */
	@Override
	public void put(T item) {
		lock.lock();
		try {
			
			Store<T> currentStore = Arrays.stream(stores).filter((s) -> s.remainingCapacity() > 0).findFirst().orElse(null);
			
			while(currentStore == null) {
				notFullCondition.await();
				currentStore = Arrays.stream(stores).filter((s) -> s.size() < s.capacity()).findFirst().orElse(null);
			}
			currentStore.put(item);
			notEmptyCondition.signal();
		} catch (InterruptedException e) {
			LOGGER.error(e);
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Take.
	 *
	 * @return the t
	 */
	@Override
	public T take() {
		lock.lock();
		try {
			Store<T> currentStore = Arrays.stream(stores).filter((s) -> s.size() > 0).findFirst().orElse(null);
			
			while (currentStore == null || currentStore.size() == 0) {
				notEmptyCondition.await();
				currentStore = Arrays.stream(stores).filter((s) -> s.size() > 0).findFirst().orElse(null);
			}
			
			T item = currentStore.take();
			notFullCondition.signal();
			return item;
		} catch (InterruptedException e) {
			LOGGER.error(e);
			return null;
		} finally {
			lock.unlock();
		}
	}


	/**
	 * Take.
	 *
	 * @param item the item
	 * @return true, if successful
	 */
	@Override
	public boolean take(T item) {
		lock.lock();
		try {
			boolean success = false;
			int idx = 0;
			while (idx < stores.length && !success) {
				success = stores[idx].take(item);
				idx++;
			}
			
			notFullCondition.signal();
			
			return success;
		}
		finally {
			lock.unlock();
		}
	}

	/**
	 * Size.
	 *
	 * @return the int
	 */
	@Override
	public int size() {
		lock.lock();
		try {
			return Arrays.stream(stores).map(s -> s.size()).reduce((total, current) -> total + current).orElse(0);
		} 
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Remaining capacity.
	 *
	 * @return the int
	 */
	public int remainingCapacity() {
        lock.lock();
        try {
            int result=  capacity() - size();
            return result;
        } finally {
            lock.unlock();
        }
    }

	/**
	 * Capacity.
	 *
	 * @return the int
	 */
	@Override
	public int capacity() {
		lock.lock();
		try {
			return Arrays.stream(stores).map(s -> s.capacity()).reduce((total, current) -> total + current).orElse(0);
		} 
		finally {
			lock.unlock();
		}
	}

}
