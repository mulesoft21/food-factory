package com.sms.domain.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.domain.BaseAssemblyLineStage;
import com.sms.domain.Product;
import com.sms.exceptions.AssemblyStageException;
import com.sms.utils.CustomError;

/**
 * The Class ServingFoodStageImpl.
 */
public class ServingFoodStageImpl extends BaseAssemblyLineStage<Product, Product> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(ServingFoodStageImpl.class);
	
	/**
	 * On start.
	 */
	@Override
	public void onStart() {
		LOGGER.info(Thread.currentThread().getName() + " STARTING STAGE WORKER");
	}

	/**
	 * On running.
	 */
	@Override
	public void onRunning() {
		LOGGER.info(Thread.currentThread().getName() + " RUNNING STAGE WORKER");
		
		try {
			Product currentProduct = take().getContent();
			LOGGER.info(Thread.currentThread().getName() + " SERVING " + currentProduct);
		} catch (AssemblyStageException e) {
			handlePipelineException(e);
		}
	}

	/**
	 * On finish.
	 */
	@Override
	public void onFinish() {
		LOGGER.info(Thread.currentThread().getName() + " FINISHING STAGE WORKER");
	}

	/**
	 * Handle pipeline exception.
	 *
	 * @param e the e
	 */
	@Override
	public void handlePipelineException(Exception e) {
		LOGGER.error(CustomError.STAGE_ERROR, e);
	}

}
