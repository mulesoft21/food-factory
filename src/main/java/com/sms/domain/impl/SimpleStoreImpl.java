package com.sms.domain.impl;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sms.domain.Store;

/**
 * The Class SimpleStoreImpl.
 *
 * @param <T> the generic type
 */
public class SimpleStoreImpl<T> implements  Store<T>{

	/** The items. */
	private BlockingQueue<T> items;

	/** The capacity. */
	private int capacity;
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(SimpleStoreImpl.class);

	/**
	 * Instantiates a new simple store impl.
	 *
	 * @param capacity the capacity
	 */
	public SimpleStoreImpl(int capacity) {
		super();
		this.capacity = capacity;
		this.items = new ArrayBlockingQueue<>(capacity);
	}

	/**
	 * Put.
	 *
	 * @param item the item
	 */
	@Override
	public void put(T item) {
		try {
			items.put(item);
		} catch (InterruptedException e) {
			LOGGER.error(e);
		}
	}

	/**
	 * Take.
	 *
	 * @return the t
	 */
	@Override
	public T take() {
		return items.poll();
	}

	/**
	 * Take.
	 *
	 * @param product the product
	 * @return true, if successful
	 */
	@Override
	public boolean take(T product) {
		return items.remove(product);
	}

	/**
	 * Size.
	 *
	 * @return the int
	 */
	@Override
	public int size() {
		return items.size();
	}
	
	/**
	 * Capacity.
	 *
	 * @return the int
	 */
	@Override
	public int capacity() {
		return capacity;
	}

	/**
	 * Remaining capacity.
	 *
	 * @return the int
	 */
	@Override
	public int remainingCapacity() {
		return capacity - items.size(); 
	}
}
