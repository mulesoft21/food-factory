package com.sms.domain;

import com.sms.exceptions.AssemblyStageException;

/**
 * The Interface AssemblyLineStage.
 *
 * @param <I> the generic type
 * @param <O> the generic type
 */
public interface AssemblyLineStage <I,O> extends PipelineStage<I,O>{
	
	/**
	 * Put after.
	 *
	 * @param output the output
	 * @throws AssemblyStageException the assembly stage exception
	 */
	void putAfter(TrackeableMessage<O> output) throws AssemblyStageException;
	
	/**
	 * Take.
	 *
	 * @return the trackeable message
	 * @throws AssemblyStageException the assembly stage exception
	 */
	TrackeableMessage<I> take() throws AssemblyStageException;
}
