package com.sms.exceptions;

/**
 * The Class AssemblyStageException.
 */
public class AssemblyStageException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6414016561828967392L;

	/**
	 * Instantiates a new assembly stage exception.
	 */
	public AssemblyStageException() {
		super();
	}

	/**
	 * Instantiates a new assembly stage exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public AssemblyStageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new assembly stage exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public AssemblyStageException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new assembly stage exception.
	 *
	 * @param message the message
	 */
	public AssemblyStageException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new assembly stage exception.
	 *
	 * @param cause the cause
	 */
	public AssemblyStageException(Throwable cause) {
		super(cause);
	}
	
	

}
