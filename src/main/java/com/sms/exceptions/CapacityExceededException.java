package com.sms.exceptions;

/**
 * The Class CapacityExceededException.
 */
public class CapacityExceededException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1566054970247837981L;

	/**
	 * Instantiates a new capacity exceeded exception.
	 */
	public CapacityExceededException() {
		super();
	}

	/**
	 * Instantiates a new capacity exceeded exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public CapacityExceededException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new capacity exceeded exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public CapacityExceededException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new capacity exceeded exception.
	 *
	 * @param message the message
	 */
	public CapacityExceededException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new capacity exceeded exception.
	 *
	 * @param cause the cause
	 */
	public CapacityExceededException(Throwable cause) {
		super(cause);
	}
	
	

}
