package com.sms;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.sms.domain.TrackeableMessage;

public class TrackeableMessageTest {
	
	private TrackeableMessage<String> createTrackeableMessage() {
		TrackeableMessage<String> tm = new TrackeableMessage<String>("Msg 1", "CHANNEL");
		return tm;
	}
	
	public void constructor_ShouldBeCreateAnInstance() {
		TrackeableMessage<String> tm = createTrackeableMessage();
		assertNotNull(tm);
		assertEquals(tm.getContent(), "Msg 1");
		assertEquals(tm.getChannel(), "CHANNEL");
	}
	
	public void setters_ShouldBeSetAttr() {
		TrackeableMessage<String> tm = createTrackeableMessage();
		tm.setContent("change");
		assertEquals(tm.getContent(), "change");
		tm.setChannel("OTHER_CHANNEL");
		assertEquals(tm.getContent(), "OTHER_CHANNEL");
	}

}
