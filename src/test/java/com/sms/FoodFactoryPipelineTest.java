package com.sms;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.sms.domain.FoodFactoryPipeline;

public class FoodFactoryPipelineTest {
	
	@Test
	public void build_ShouldBeCreateInstance() {
    	FoodFactoryPipeline p = new FoodFactoryPipeline.Builder()
    			.withChannelBusCapacity(12)
    			.withOvenCapacity(11)
    			.withStoreCapacity(10)
    			.withStoresCount(9)
    			.withOvensCount(8)
    			.build();
    	assertNotNull(p);
    	assertEquals(12, p.getChannelBusCapacity());
    	assertEquals(11, p.getOvenCapacity());
    	assertEquals(10, p.getStoreCapacity());
    	assertEquals(9, p.getStoresCount());
    	assertEquals(8,  p.getOvensCount());
	}

}
