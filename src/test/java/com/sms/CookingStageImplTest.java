package com.sms;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.sms.domain.Oven;
import com.sms.domain.Product;
import com.sms.domain.Store;
import com.sms.domain.TrackeableMessage;
import com.sms.domain.impl.CookingStageImpl;
import com.sms.domain.impl.MultiStoreImpl;
import com.sms.domain.impl.OvenImpl;
import com.sms.domain.impl.ProductImpl;
import com.sms.domain.impl.SimpleStoreImpl;
import com.sms.utils.MultichannelBlockingQueue;

public class CookingStageImplTest {
	
	private static final int M_OVENS = 2;
	
	private static final int N_STORES = 1;
	
	private static final double OVEN_CAPACITY = 100;
	
	private static final int STORE_CAPACITY = 20;
	
	private static final int CHANNEL_BUS_CAPACITY = 1;
	
	private static final String CHANNEL = "channel";
	
	private Product createProductOne() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 90, "Product1");
		return p;
	}
	
	@SuppressWarnings("unchecked")
	private  Oven<TrackeableMessage<Product>>[] createOvens() {
		Oven<TrackeableMessage<Product> >[] ovens = new Oven[M_OVENS];
		IntStream.range(0, M_OVENS).forEach((idx) -> ovens[idx] = new OvenImpl(OVEN_CAPACITY));
		return ovens;
	}
	
	@SuppressWarnings("unchecked")
	private Store<TrackeableMessage<Product>> createStore() {
		Store<TrackeableMessage<Product>>[] stores = new Store[N_STORES];
		IntStream.range(0, N_STORES).forEach((idx) -> stores[idx] = new SimpleStoreImpl<TrackeableMessage<Product>>(STORE_CAPACITY));
		return new MultiStoreImpl<TrackeableMessage<Product>>(stores);
	}
	
	private MultichannelBlockingQueue<TrackeableMessage<Product>> createBuffer() {
		String[] channelsNames = {CHANNEL};
		return new MultichannelBlockingQueue<TrackeableMessage<Product>>(CHANNEL_BUS_CAPACITY, channelsNames);
	}
	
	private CookingStageImpl createCookingStage() {
		return new CookingStageImpl(createOvens(), createStore());
	}
	
	@Test
	public void putAfter_ShouldBePutAnElementInOuputBuffer() throws InterruptedException {
		CookingStageImpl stage = createCookingStage();
		MultichannelBlockingQueue<TrackeableMessage<Product>> inputBuffer = createBuffer();
		MultichannelBlockingQueue<TrackeableMessage<Product>> outputBuffer = createBuffer();
		stage.init(inputBuffer, outputBuffer, CHANNEL, CHANNEL);
		new Thread(stage).start();
		TrackeableMessage<Product> inputMsg = new TrackeableMessage<Product>(createProductOne(), CHANNEL);
		inputBuffer.put(inputMsg, CHANNEL);
		TrackeableMessage<Product> outputMsg = outputBuffer.take(CHANNEL);
		Thread.sleep(500);
		assertEquals(inputMsg, outputMsg);
	}
	
	@Test
	public void take_ShouldBeTakeAnElementFromInputBuffer() throws InterruptedException {
		CookingStageImpl stage = createCookingStage();
		MultichannelBlockingQueue<TrackeableMessage<Product>> inputBuffer = createBuffer();
		MultichannelBlockingQueue<TrackeableMessage<Product>> outputBuffer = createBuffer();
		stage.init(inputBuffer, outputBuffer, CHANNEL, CHANNEL);
		new Thread(stage).start();
		TrackeableMessage<Product> inputMsg = new TrackeableMessage<Product>(createProductOne(), CHANNEL);
		inputBuffer.put(inputMsg, CHANNEL);
		Thread.sleep(500);
		assertTrue(inputBuffer.isEmpty(CHANNEL));
	}
}
