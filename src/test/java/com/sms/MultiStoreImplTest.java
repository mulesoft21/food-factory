package com.sms;

import org.junit.jupiter.api.Test;

import com.sms.domain.Store;
import com.sms.domain.impl.MultiStoreImpl;
import com.sms.domain.impl.SimpleStoreImpl;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;

public class MultiStoreImplTest extends MultithreadedTestCase{
	
	private Store<Integer> store;
	
	private MultiStoreImpl<Integer> createMultiStoreWithTwoClusters(int capacity){
		Store<Integer> store1 = new SimpleStoreImpl<Integer>(capacity);
		Store<Integer> store2= new SimpleStoreImpl<Integer>(capacity);
		Store<Integer>[] stores = new Store[2];
		stores[0] = store1;
		stores[1] = store2;
		return new MultiStoreImpl<Integer>(stores);
	}
	
	@Test
	public void remainingCapacity_ShouldBeReturnRemainingCapacity() throws InterruptedException {
		Store<Integer> store = createMultiStoreWithTwoClusters(1);
		store.put(1);
		assertEquals(1, store.remainingCapacity());
	}
	
	@Test
	public void put_ShouldBeAddAnElement() throws InterruptedException {
		Store<Integer> store = createMultiStoreWithTwoClusters(1);
		store.put(1);
		assertEquals(1, store.remainingCapacity());
	}
	
	@Test
	public void take_ShouldBeRemoveAnElementByEquals() throws InterruptedException {
		Integer item = 13;
		Store<Integer> store = createMultiStoreWithTwoClusters(1);
		store.put(item);
		assertTrue(store.take(item));
		assertEquals(2, store.remainingCapacity());
	}
	
	@Test
	public void take_ShouldBeRemoveAnElement() throws InterruptedException {
		Integer item = 13;
		Store<Integer> store = createMultiStoreWithTwoClusters(1);
		store.put(item);
		assertEquals(item, store.take());
		assertEquals(2, store.remainingCapacity());
	}
	
	@Override
	public void initialize() {
		store = createMultiStoreWithTwoClusters(1);
	}

	public void threadPut() throws InterruptedException {
		store.put(42);
		store.put(17);
		store.put(10);
		assertTick(1);
	}

	public void thread2() throws InterruptedException {
		waitForTick(1);
		assertEquals(Integer.valueOf(42), store.take());
		assertEquals(Integer.valueOf(17), store.take());
		waitForTick(2);
		assertEquals(Integer.valueOf(10), store.take());
	}

	@Override
	public void finish() {
		assertTrue(store.remainingCapacity() == 2);
	}

	@Test
	public void putAndTake_ShouldBeSync() throws Throwable {
		TestFramework.runOnce(new MultiStoreImplTest());
	}
}
