package com.sms;

import org.junit.jupiter.api.Test;

import com.sms.domain.Store;
import com.sms.domain.impl.SimpleStoreImpl;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;

public class SimpleStoreImplTest extends MultithreadedTestCase {

	private Store<Integer> store;

	@Test
	public void remainingCapacity_ShouldBeReturnRemainingCapacity() throws InterruptedException {
		Store<Integer> store = new SimpleStoreImpl<>(2);
		store.put(1);
		assertEquals(1, store.remainingCapacity());
	}

	@Test
	public void put_ShouldBeAddAnElement() throws InterruptedException {
		Store<Integer> store = new SimpleStoreImpl<>(1);
		store.put(1);
		assertEquals(0, store.remainingCapacity());
	}

	@Test
	public void take_ShouldBeRemoveAnElementByEquals() throws InterruptedException {
		Integer item = 13;
		Store<Integer> store = new SimpleStoreImpl<>(1);
		store.put(item);
		assertTrue(store.take(item));
		assertEquals(1, store.remainingCapacity());
	}

	@Test
	public void take_ShouldBeRemoveAnElement() throws InterruptedException {
		Integer item = 13;
		Store<Integer> store = new SimpleStoreImpl<>(1);
		store.put(item);
		assertEquals(item, store.take());
		assertEquals(1, store.remainingCapacity());
	}

	@Override
	public void initialize() {
		store = new SimpleStoreImpl<>(1);
	}

	public void threadPut() throws InterruptedException {
		store.put(42);
		store.put(17);
		assertTick(1);
	}

	public void thread2() throws InterruptedException {
		waitForTick(1);
		assertEquals(Integer.valueOf(42), store.take());
		waitForTick(2);
		assertEquals(Integer.valueOf(17), store.take());
	}

	@Override
	public void finish() {
		assertTrue(store.remainingCapacity() == 1);
	}

	@Test
	public void putAndTake_ShouldBeSync() throws Throwable {
		TestFramework.runOnce(new SimpleStoreImplTest());
	}
}
