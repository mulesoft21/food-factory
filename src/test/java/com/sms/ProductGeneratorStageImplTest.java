package com.sms;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import com.sms.domain.Product;
import com.sms.domain.TrackeableMessage;
import com.sms.domain.impl.ProductGeneratorStageImpl;
import com.sms.domain.impl.ProductImpl;
import com.sms.utils.MultichannelBlockingQueue;

public class ProductGeneratorStageImplTest {

	private static final int CHANNEL_BUS_CAPACITY = 1;
	
	private static final String CHANNEL = "channel";
	
	private Product createProductOne() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 90, "Product1");
		return p;
	}
	
	private MultichannelBlockingQueue<TrackeableMessage<Product>> createBuffer() {
		String[] channelsNames = {CHANNEL};
		return new MultichannelBlockingQueue<TrackeableMessage<Product>>(CHANNEL_BUS_CAPACITY, channelsNames);
	}
	
	private ProductGeneratorStageImpl createProductGeneratorStage() {
		return new ProductGeneratorStageImpl();
	}
	
	@Test
	public void shouldBeGenerateElementsInOuputBuffer() throws InterruptedException {
		ProductGeneratorStageImpl stage = createProductGeneratorStage();
		MultichannelBlockingQueue<TrackeableMessage<Product>> outputBuffer = createBuffer();
		stage.init(null, outputBuffer, CHANNEL, CHANNEL);
		new Thread(stage).start();
		TrackeableMessage<Product> inputMsg = new TrackeableMessage<Product>(createProductOne(), CHANNEL);
		TrackeableMessage<Product> outputMsg = outputBuffer.take(CHANNEL);
		Thread.sleep(500);
		assertNotNull(outputMsg);
	}

}
