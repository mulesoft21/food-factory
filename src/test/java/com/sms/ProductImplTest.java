package com.sms;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import com.sms.domain.impl.ProductImpl;


public class ProductImplTest {
	
	private ProductImpl createProductOne() {
		return new ProductImpl(Duration.ofMillis(500), 100.00, "TestProduct1");
	}
	
	private ProductImpl createProductTwo() {
		return new ProductImpl(Duration.ofMillis(1000), 100.00, "TestProduct2");
	}
	
	private ProductImpl createProductWithStartTime() {
		ProductImpl p = new ProductImpl(Duration.ofMillis(1000), 100.00, "TestProduct2");
		p.setCookingStartTime(LocalTime.now());
		return p;
	}

	@Test
	public void getCookTime_ShouldBeReturnValue() {
		ProductImpl p = createProductOne();
		assertEquals(p.getCookTime(), Duration.ofMillis(500));
	}
	
	@Test
	public void setCookTime_ShouldBeSetValue() {
		ProductImpl p = createProductOne();
		p.setName("changedName");
		assertEquals(p.getName(), "changedName");
	}
	
	@Test
	public void setCookingStartTime_ShouldBeSetValue() {
		ProductImpl p = createProductOne();
		LocalTime lt = LocalTime.now();
		p.setCookingStartTime(lt);
		assertEquals(lt, p.getCookingStartTime());
	}
	
	@Test
	public void getSize_ShouldBeReturnValue() {
		ProductImpl p = createProductTwo();
		assertEquals(p.getSize(), 100);
	}
	
	@Test
	public void isCooked_ShouldBeDetectIfProductIsCooked() throws InterruptedException {
		ProductImpl p = createProductWithStartTime();
		Thread.sleep(1000);
		assertTrue(p.isCooked());
	}
	
	

}
