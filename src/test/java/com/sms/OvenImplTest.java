package com.sms;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import com.sms.domain.Oven;
import com.sms.domain.Product;
import com.sms.domain.TrackeableMessage;
import com.sms.domain.impl.OvenImpl;
import com.sms.domain.impl.ProductImpl;
import com.sms.exceptions.CapacityExceededException;

public class OvenImplTest {
	
	private static final String CHANNEL = "channel";
	
	private Product createProductOne() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 90, "Product1");
		return p;
	}
	
	private Product createProductTwo() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 90, "Product2");
		return p;
	}
	
	private Product createBigProduct() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 140, "Big product");
		return p;
	}
	
	@Test
	public void  getSize_ShouldBeReturnSizeInCm2() throws CapacityExceededException {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.put(new TrackeableMessage<Product>(createProductTwo(), CHANNEL));
		assertEquals(oven.getSize(), 100);
	}
	
	@Test
	public void put_ShouldBeAddAnElement() throws CapacityExceededException {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.put(new TrackeableMessage<Product>(createProductTwo(), CHANNEL));
	}
	
	@Test
	public void put_ShouldBeThrowCapacityExceededException() {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		assertThrows(CapacityExceededException.class, ()-> oven.put(new TrackeableMessage<Product>(createBigProduct(), CHANNEL)));
		
		assertThrows(CapacityExceededException.class, ()-> { 
			oven.put(new TrackeableMessage<Product>(createProductOne(), CHANNEL));
			oven.put(new TrackeableMessage<Product>(createProductTwo(), CHANNEL));
		});
	}
	
	@Test
	public void take_ShouldBeRemoveAnElement() throws CapacityExceededException {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		TrackeableMessage<Product> item = new TrackeableMessage<>(createProductOne(), CHANNEL);
		oven.put(item);
		assertTrue(oven.take(item));
	}
	
	@Test
	public void turnOn_ShouldBeTurnOnOven() {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.turnOn();
		assertTrue(oven.isOn());
	}

	@Test
	public void turnOnWithDuration_ShouldBeTurnOnOvenOverDuration() throws InterruptedException{
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.turnOn(Duration.ofSeconds(1));
		assertTrue(oven.isOn());
		Thread.sleep(2000);
		assertTrue(!oven.isOn());
	};
	
	@Test
	public void turnOff_ShouldBeTurnOffOven() {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.turnOn();
		oven.turnOff();
		assertTrue(!oven.isOn());
	};
	
	@Test
	public void turnOff_ShouldBeResetOvenState() throws CapacityExceededException {
		Oven<TrackeableMessage<Product>> oven = new OvenImpl(100);
		oven.turnOn();
		oven.put(new TrackeableMessage<Product>(createProductOne(), CHANNEL));
		oven.turnOff();
		assertTrue(!oven.isOn());
		oven.turnOn();
		oven.put(new TrackeableMessage<Product>(createProductTwo(), CHANNEL));
	};

}
