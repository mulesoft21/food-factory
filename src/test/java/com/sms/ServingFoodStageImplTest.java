package com.sms;

import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import com.sms.domain.Product;
import com.sms.domain.TrackeableMessage;
import com.sms.domain.impl.ProductImpl;
import com.sms.domain.impl.ServingFoodStageImpl;
import com.sms.utils.MultichannelBlockingQueue;

public class ServingFoodStageImplTest {
	private static final int CHANNEL_BUS_CAPACITY = 1;
	
	private static final String CHANNEL = "channel";
	
	private Product createProductOne() {
		Product p = new ProductImpl(Duration.ofSeconds(1), 90, "Product1");
		return p;
	}
	
	private MultichannelBlockingQueue<TrackeableMessage<Product>> createBuffer() {
		String[] channelsNames = {CHANNEL};
		return new MultichannelBlockingQueue<TrackeableMessage<Product>>(CHANNEL_BUS_CAPACITY, channelsNames);
	}
	
	private ServingFoodStageImpl createServingFoodStage() {
		return new ServingFoodStageImpl();
	}
	
	@Test
	public void take_ShouldBeTakeAnElementFromInputBuffer() throws InterruptedException {
		ServingFoodStageImpl stage = createServingFoodStage();
		MultichannelBlockingQueue<TrackeableMessage<Product>> inputBuffer = createBuffer();
		stage.init(inputBuffer, null, CHANNEL, CHANNEL);
		new Thread(stage).start();
		TrackeableMessage<Product> inputMsg = new TrackeableMessage<Product>(createProductOne(), CHANNEL);
		inputBuffer.put(inputMsg, CHANNEL);
		Thread.sleep(500);
		assertTrue(inputBuffer.isEmpty(CHANNEL));
	}
}
